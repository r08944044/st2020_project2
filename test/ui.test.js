const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true;
const TimeOutLimit = 30000;

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})

// 2
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headlss: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    let title = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(title).toBe('Join');
    
    await browser.close();
}, TimeOutLimit)

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'Show Lo', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(1600);
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    
    await browser.close();
}, TimeOutLimit)

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', '', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', '', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(1600);
    await page.screenshot({path: 'test/screenshots/oops.png'});
    
    await browser.close();
}, TimeOutLimit)

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'Show Lo', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page.keyboard.press('Enter');
    
    await page.waitFor(3000);
    let message = await page.$eval('.message__body > p', (content) => content.innerHTML);
    expect(message).toBe('Hi Show Lo, Welcome to the chat app');
    
    await browser.close();
}, TimeOutLimit)

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');
    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('Multiplayer')
    
    await browser.close();
}, TimeOutLimit)

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitForSelector('#users > ol > li');
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'Show Lo', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(3000);
    
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();
    await page2.goto(url);
    await page2.type('body > div >form > div:nth-child(2) > input[type=text]', 'Linda Chien', {delay: 100});
    await page2.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page2.keyboard.press('Enter');
    await page2.waitFor(3000);
    
    let member_1 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    let member_2 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member_1).toBe('Show Lo');
    expect(member_2).toBe('Linda Chien');
    
    let member2_1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    let member2_2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member2_1).toBe('Show Lo');
    expect(member2_2).toBe('Linda Chien');
    
    await browser.close();
    await browser2.close();
}, TimeOutLimit)

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'Show Lo', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(3000);
    
    let title = await page.$eval('body > div:nth-child(2) > div > form > button', (content) => content.innerHTML);
    expect(title).toBe('Send');
    
    await browser.close();
}, TimeOutLimit)

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'Show Lo', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(3000);
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'I wanna know, can you do it or not?', {delay: 100});
    await page.keyboard.press('Enter');
    
    let title = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    let body = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    expect(title).toBe('Show Lo');
    expect(body).toBe('I wanna know, can you do it or not?');
    
    await browser.close();
}, TimeOutLimit)

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(3000);
    
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();
    await page2.goto(url);
    await page2.type('body > div >form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div >form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter');
    await page2.waitFor(3000);
    
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hi', {delay: 100});
    await page.keyboard.press('Enter');
    await page2.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hello', {delay: 100});
    await page2.keyboard.press('Enter');
    
    let title_1 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
    });
    let body_1 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
    });
    let title_2 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(1) > h4').innerHTML;
    });
    let body_2 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });
    expect(title_1).toBe('John');
    expect(body_1).toBe('Hi');
    expect(title_2).toBe('Mike');
    expect(body_2).toBe('Hello');
    
    let title2_1 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    let body2_1 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    let title2_2 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
    });
    let body2_2 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
    });
    expect(title2_1).toBe('John');
    expect(body2_1).toBe('Hi');
    expect(title2_2).toBe('Mike');
    expect(body2_2).toBe('Hello');
    
    await browser.close();
    await browser2.close();
}, TimeOutLimit)

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'Show Lo', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(3000);
    
    let title = await page.$eval('body > div:nth-child(2) > div > button', (content) => content.innerHTML);
    expect(title).toBe('Send location');
    
    await browser.close();
}, TimeOutLimit)

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    
    const context = browser.defaultBrowserContext();
    await context.overridePermissions('http://localhost:3000/', ['geolocation']);
    await page.setGeolocation({latitude: 25.01717, longitude: 121.53676});
    
    await page.type('body > div >form > div:nth-child(2) > input[type=text]', 'Show Lo', {delay: 100});
    await page.type('body > div >form > div:nth-child(3) > input[type=text]', 'Multiplayer', {delay: 100});
    await page.keyboard.press('Enter');
    await page.waitFor(3000);
    
    await page.click('#send-location');
    await page.waitFor(3000);
    let body = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > div > iframe').src;
    });
    api = new URL(body);
    expect(api.hostname).toBe('maps.google.com');

    await browser.close();
}, TimeOutLimit)
